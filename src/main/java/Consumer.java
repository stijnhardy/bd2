import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Deprecated
public class Consumer implements Runnable{
    private final DateTimeFormatter stampformat;
    ObjectMapper mapper = new ObjectMapper();
    String EAN;

    public Consumer(String EAN) {
        this.EAN = EAN;
        mapper.registerModule(new JavaTimeModule());
        String format = "yyyyMMddHHmmss";
        stampformat = DateTimeFormatter.ofPattern(format);
    }

    private int createWattage() {
        int increasePattern = LocalDateTime.now().getSecond() < 30 ? 100 : 500;
        long wattage =  Math.round(Math.random() * increasePattern);
        return (int)wattage;
    }

    public void run() {
        System.out.println("Consumer thread started for EAN: " + EAN);

        while(true) {
            LocalDateTime timestamp = LocalDateTime.now();
            Consumption consumption = new Consumption();
            consumption.setWattage(createWattage());
            consumption.setEan(EAN);
            consumption.setTime(timestamp);
            Storage storage = StorageOptions.getDefaultInstance().getService();
            BlobId blobId = BlobId.of("be_kdg_sh_bigdata", "CONS" + EAN +  timestamp.format(stampformat) + ".json");
            BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("application/json").build();
            try {
                storage.create(blobInfo, mapper.writeValueAsBytes(consumption));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(Generate.INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
