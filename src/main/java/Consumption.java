import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Deprecated
@Setter
@Getter
public class Consumption {
    String ean;
    int wattage;
    LocalDateTime time;
}
