import java.util.ArrayList;
import java.util.List;

public class Generate {

    public static final int INTERVAL = 10000;
    public static String bucketName = "be_kdg_sh_bigdata";

    public static void main(String[] args) {
        String basepath = "Z:\\STIJN\\BIGDATA2";
        String gcCredentials = "C:\\Users\\stijn\\Desktop\\bigdata2-353420-131efe69dcaa.json";
        if (args.length > 0) {
            basepath = args[0];
            gcCredentials = args[1];
            bucketName = args[2];
        }
        StorageService storageService = new StorageService(false, basepath, gcCredentials, bucketName, "eans");
        storageService.storeJson(generateEans());
        Thread t1 = new Thread(new Unit(INTERVAL, generateEans(), 1, basepath, gcCredentials, bucketName));
        t1.start();
        Thread t2 = new Thread(new Unit(INTERVAL, generateEans(), 2, basepath, gcCredentials, bucketName));
        t2.start();
        Thread t3 = new Thread(new Unit(INTERVAL, generateEans(),3, basepath, gcCredentials, bucketName));
        t3.start();
        System.out.println("threads started");
    }


    private static List<EAN> generateEans() {
        List<EAN> eans = new ArrayList<>();
        eans.add(new EAN("7699813145621","1000", Type.CONSUMER));
        eans.add(new EAN("9709468929373","1000", Type.PRODUCER));
        eans.add(new EAN("6632383820322","1000", Type.CONSUMER));
        eans.add(new EAN("6636166476867","3000", Type.PRODUCER));
        eans.add(new EAN("9133902082919","3000", Type.CONSUMER));
        eans.add(new EAN("9530635578472","3000", Type.CONSUMER));
        eans.add(new EAN("7589986278380","2000", Type.CONSUMER));
        eans.add(new EAN("4773613364963","2000", Type.PRODUCER));
        eans.add(new EAN("3002522496160","2000", Type.CONSUMER));
        eans.add(new EAN("6213620447672","2000", Type.PRODUCER));
        return eans;
    };
}
