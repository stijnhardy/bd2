import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@AllArgsConstructor
public class Measurement {
    String id;
    int wattage;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    LocalDateTime time;
}
