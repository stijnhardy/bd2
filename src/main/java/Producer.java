import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.cloud.storage.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Deprecated
public class Producer implements Runnable{
    private final DateTimeFormatter stampformat;
    ObjectMapper mapper = new ObjectMapper();
    String id;

    public Producer(String id) {
        this.id = id;
        mapper.registerModule(new JavaTimeModule());
        String format = "yyyyMMddHHmmss";
        stampformat = DateTimeFormatter.ofPattern(format);
    }

    private int createWattage() {
        int increasePattern = LocalDateTime.now().getSecond() < 30 ? 100 : 500;
        long wattage =  Math.round(Math.random() * increasePattern);
        return (int)wattage;
    }

    public void run() {
        System.out.println("Producer hread started for EAN: " + id);

        while(true) {
            LocalDateTime timestamp = LocalDateTime.now();
            Production production = new Production();
            production.setWattage(createWattage());
            production.setProductionId(id);
            production.setTime(timestamp);
            Storage storage = StorageOptions.getDefaultInstance().getService();
            BlobId blobId = BlobId.of("be_kdg_sh_bigdata", "PROD" + id +  timestamp.format(stampformat) + ".json");
            BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("application/json").build();
            try {
                storage.create(blobInfo, mapper.writeValueAsBytes(production));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(Generate.INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
