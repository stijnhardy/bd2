import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@Deprecated
public class Production {
    String productionId;
    int wattage;
    LocalDateTime time;
}
