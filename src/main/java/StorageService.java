import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

import java.io.*;
import java.time.format.DateTimeFormatter;

public class StorageService {
    private OutputStreamWriter osw;
    private File file;
    private boolean isGcMode;
    private String basePath;
    private String GCCredentialsLocation;
    private String bucketName;
    private ObjectMapper mapper = new ObjectMapper();
    private DateTimeFormatter stampformat;

    public StorageService(boolean isGcMode, String basePath, String GCCredentialsLocation, String bucketName, String fileName) {
        this.isGcMode = isGcMode;
        this.basePath = basePath;
        this.GCCredentialsLocation = GCCredentialsLocation;
        this.bucketName = bucketName;

        file = new File(basePath, fileName + ".json" );

        mapper.registerModule(new JavaTimeModule());
        String format = "yyyyMMddHHmmss";
        stampformat = DateTimeFormatter.ofPattern(format);
    }

    public void storeJson(Object data) {
        try {
            //mapper.writeValue(file, data);
            //SequenceWriter writer = mapper.writer().withRootValueSeparator("\n").writeValues(file);
            //writer.write(data);
            //JSONObject a = mapper.writer().
            //try (SequenceWriter seq = mapper.writer().withRootValueSeparator("\n").writeValues(file)) {
                //seq.write(data);
            //}
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(file, true);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            osw = new OutputStreamWriter(fos);
            osw.write(mapper.writeValueAsString(data));
            osw.write("\n");
            osw.flush();
            if(isGcMode) {
                saveInGC("power/" + file.getName() + ".json", new FileInputStream(file));
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private void saveInGC(String fileName, FileInputStream file) throws IOException {
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream(GCCredentialsLocation));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        BlobInfo blobInfo = BlobInfo.newBuilder(BlobId.of(bucketName, fileName)).setContentType("text/json").build();
        storage.create(blobInfo, file);
        //We can use this to create the bucket if it were not yet present
        //Bucket bucket = storage.create(BucketInfo.of(bucketName));
        //Blob blob = bucket.create(blobName, data.getBytes(UTF_8), "text/json");
    }


}
