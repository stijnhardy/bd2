import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Unit implements Runnable{
    private final int id;
    private int interval;
    private List<EAN> eans;
    private StorageService storageService;

    private ObjectMapper mapper = new ObjectMapper();
    private DateTimeFormatter stampformat;

    public Unit(int interval, List<EAN> eans, int id, String basepath, String gcCredentials, String bucketName) {
        this.interval = interval;
        this.eans = eans;
        this.id = id;

        storageService = new StorageService(false, basepath, gcCredentials, bucketName, String.valueOf(id));

        mapper.registerModule(new JavaTimeModule());
        String format = "yyyyMMddHHmmss";
        stampformat = DateTimeFormatter.ofPattern(format);
    }

    public void run() {
        while(true) {
            for (EAN ean : eans) {
                LocalDateTime timestamp = LocalDateTime.now();
                Measurement measurement = new Measurement(ean.getId(), createWattage(), timestamp);
                storageService.storeJson(measurement);
                //storageService.storeJson(ean.getId() + "-" + timestamp.format(stampformat) + "json", measurement, true);
            }
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private int createWattage() {
        int increasePattern = LocalDateTime.now().getSecond() < 30 ? 100 : 500;
        long wattage =  Math.round(Math.random() * increasePattern);
        return (int)wattage;
    }
}
